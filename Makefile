DEVBOX_CONTAINER=cookiecutter_go_devbox

.PHONY: dev
dev: lint test

.PHONY: install_dev
install_dev: init
	$(call CMD, pip install -r requirements_dev.txt)

.PHONY: lint
lint: install_dev
	$(call CMD, pycodestyle .)
	$(call CMD, pydocstyle hooks)

# test targets
test: unittest functionaltest

.PHONY: unittest
unittest: install_dev
	$(call CMD, pytest --verbose tests/unit)

.PHONY: functionaltest
functionaltest: install_dev
	$(call CMD, pytest --verbose tests/functional)

.PHONE: generate
generate: install_dev
	$(call CMD,rm -rf ./.tmp/jujube)
	$(call CMD,mkdir -p ./.tmp)
	$(call CMD,cd ./.tmp && cookiecutter --no-input ./..)

# targets to bring up development environment
DOCKER_COMPOSE_CMD = docker-compose -f dev/docker/docker-compose.yml -p cookiecutter_go
ifdef NO_DOCKER
	DOCKER_COMPOSE_CMD=echo $DOCKER_COMPOSE_CMD
endif

.PHONY: init
init: .tmp/init
.tmp/init:
	$(DOCKER_COMPOSE_CMD) up -d
	mkdir -p ./.tmp && touch ./.tmp/init

.PHONY: clean
clean:
	$(DOCKER_COMPOSE_CMD) kill
	$(DOCKER_COMPOSE_CMD) down --rmi local -v
	rm -f ./.tmp/init

-include dev/env.mk
