# -*- coding: utf-8 -*-

import pytest


class SetupRulesMk:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(
                "build", "rules.mk").open() as f:
            return f.readlines()


class TestRulesDefault(SetupRulesMk):
    def test_has_dev_host_env_mk(self, template):
        assert template.project_path.joinpath(
            "build", "rules.mk").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "# make targets to be executed inside the devbox container.\n",
        "PROGS = cmd/jujube/jujube\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestRulesCustom(SetupRulesMk):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-new"}

    lines = [
        "PROGS = cmd/jujube-new/jujube-new\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
