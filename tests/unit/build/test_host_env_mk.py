# -*- coding: utf-8 -*-

import pytest


class SetupHostEnvMk:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(
                "build", "host_env.mk").open() as f:
            return f.readlines()


class TestHostEnvDefault(SetupHostEnvMk):
    def test_has_dev_host_env_mk(self, template):
        assert template.project_path.joinpath(
            "build", "host_env.mk").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "# make targets to be executed from outside the devbox container.\n",

        # docker command wrapper definition (2 lines)
        "DOCKER_CMD = docker exec -t $(DEVBOX_CONTAINER) bash -c "
        "\"cd /src && $(1)\"\n",

        # docker-compose command (2 lines)
        "DOCKER_COMPOSE_CMD = docker-compose "
        "-f deployments/docker/docker-compose.yml "
        "-p jujube\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestHostEnvCustom(SetupHostEnvMk):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-new"}

    lines = [
        # docker-compose command (2 lines)
        "DOCKER_COMPOSE_CMD = docker-compose "
        "-f deployments/docker/docker-compose.yml "
        "-p jujube_new\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
