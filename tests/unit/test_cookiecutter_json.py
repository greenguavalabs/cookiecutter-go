# -*- coding: utf-8 -*-

import json
import pytest

from collections import OrderedDict


class SetupCookiecutterJson:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath("cookiecutter.json").open() as f:
            return f.readlines()


class TestCookiecutterJson(SetupCookiecutterJson):
    def test_has_file(self, template):
        assert template.project_path.joinpath("cookiecutter.json").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    def test_has_content(self, template, file):
        context = json.loads(file[0], object_pairs_hook=OrderedDict)
        context.pop("_template", None)  # pytest_cookies does not capture it
        context.pop("_output_dir", None)  # pytest_cookies does not capture it
        assert template.context == context
