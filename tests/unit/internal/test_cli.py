# -*- coding: utf-8 -*-

import pytest


class SetupCli:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)


class TestCliDefault(SetupCli):
    def test_has_cli_folder(self, template):
        assert template.project_path.joinpath("internal", "cli").is_dir()


class TestCliExplicitEnabled(SetupCli):
    @pytest.fixture()
    def context(self):
        return {"cli": "yes"}

    def test_has_cli_folder(self, template):
        assert template.project_path.joinpath("internal", "cli").is_dir()


class TestCliDisabled(SetupCli):
    @pytest.fixture()
    def context(self):
        return {"cli": "no"}

    def test_does_not_have_cli_folder(self, template):
        assert not template.project_path.joinpath("internal", "cli").exists()
