# -*- coding: utf-8 -*-

import pytest


class SetupAllTestGo:
    @pytest.fixture()
    def context(self):
        return {"cli": "yes"}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture(params=[
        "flag_test.go",
        "rune_test.go",
    ])
    def file(self, request, template):
        with template.project_path.joinpath(
                "internal", "cli", "action", request.param).open() as f:
            return f.readlines()


class TestAllTestGoDefault(SetupAllTestGo):
    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "\t. \"github.com/greenguavalabs/jujube/internal/cli/action\"\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
