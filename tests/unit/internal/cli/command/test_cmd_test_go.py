# -*- coding: utf-8 -*-

import pytest


class SetupCmdTestGo:
    @pytest.fixture()
    def context(self):
        return {"cli": "yes"}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(
                "internal", "cli", "command", "cmd_test.go").open() as f:
            return f.readlines()


class TestCmdTestGoDefault(SetupCmdTestGo):
    def test_has_file(self, template):
        assert template.project_path.joinpath(
            "internal", "cli", "command", "cmd_test.go").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "\t\"github.com/greenguavalabs/jujube/internal/cli/action\"\n",
        "\t. \"github.com/greenguavalabs/jujube/internal/cli/command\"\n",
        "\t\"github.com/greenguavalabs/jujube/internal/cli/command/greet\"\n",
        "\t\tExpect(cmd.Use).To(Equal(\"jujube\"))\n",
        "\t\tExpect(cmd.Short).To(Equal(\"jujube app\"))\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
