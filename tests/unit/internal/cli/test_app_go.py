# -*- coding: utf-8 -*-

import pytest


class SetupAppGo:
    @pytest.fixture()
    def context(self):
        return {"cli": "yes"}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(
                "internal", "cli", "app.go").open() as f:
            return f.readlines()


class TestAppGoDefault(SetupAppGo):
    def test_has_file(self, template):
        assert template.project_path.joinpath(
            "internal", "cli", "app.go").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "\t\"github.com/greenguavalabs/jujube/internal/cli/command\"\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
