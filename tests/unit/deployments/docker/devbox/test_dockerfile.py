# -*- coding: utf-8 -*-

import pytest


class SetupDockerfile:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture
    def file(self, template):
        with template.project_path.joinpath(
                "deployments", "docker", "devbox", "Dockerfile").open() as f:
            return f.readlines()


class TestDockerfileDefault(SetupDockerfile):
    def test_has_devbox_docker_file(self, template):
        assert template.project_path.joinpath(
            "deployments", "docker", "devbox", "Dockerfile").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "FROM golang:latest\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDockerfileGoVersion(SetupDockerfile):
    @pytest.fixture
    def context(self, request):
        return {"go_version": "1.10"}

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "FROM golang:1.10\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
