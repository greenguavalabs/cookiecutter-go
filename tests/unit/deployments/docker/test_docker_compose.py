# -*- coding: utf-8 -*-

import pytest


class SetupDockerCompose:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture
    def file(self, template):
        with template.project_path.joinpath(
                "deployments", "docker", "docker-compose.yml").open() as f:
            return f.readlines()


class TestDockerComposeDefault(SetupDockerCompose):
    def test_has_devbox_docker_compose(self, template):
        assert template.project_path.joinpath(
            "deployments", "docker", "docker-compose.yml").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        # service definition
        "  jujube_devbox:\n",
        "    container_name: jujube_devbox\n",

        # volume definition
        "      - ./../../:/src\n",

        # env variabes
        "      IN_CONTAINER: jujube_devbox\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDockerComposeProjectSlugHasDash(SetupDockerCompose):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-dev"}

    lines = [
        # service definition
        "  jujube_dev_devbox:\n",
        "    container_name: jujube_dev_devbox\n",

        # env variables
        "      IN_CONTAINER: jujube_dev_devbox\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
