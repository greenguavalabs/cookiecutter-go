# -*- coding: utf-8 -*-

import hashlib
import os
import pytest


class SetupCookiecutterCrc:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(".cookiecutter.crc").open() as f:
            return f.readlines()


class TestCookiecutterCrc(SetupCookiecutterCrc):
    def test_has_file(self, template):
        assert template.project_path.joinpath(".cookiecutter.crc").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    def test_has_correct_sha256_crc(self, template, file):
        for line in file:
            print(line)
            line = line.split("|")
            if len(line) != 2:
                # skip over malformed lines
                continue

            contents = template.project_path.joinpath(
                line[1].strip()).read_bytes()
            assert line[0] == hashlib.sha256(contents).hexdigest()
