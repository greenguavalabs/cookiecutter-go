# -*- coding: utf-8 -*-

import pytest


class SetupMainGo:
    @pytest.fixture()
    def project_slug(self):
        return None

    @pytest.fixture()
    def context(self, project_slug):
        if project_slug is None:
            return None
        else:
            return {"project_slug": project_slug}

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template, project_slug):
        if project_slug is None:
            project_slug = "jujube"

        with template.project_path.joinpath(
                "cmd", project_slug, "main.go").open() as f:
            return f.readlines()


class TestMainGoDefault(SetupMainGo):
    def test_has_main_go(self, template):
        assert template.project_path.joinpath(
            "cmd", "jujube", "main.go").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "\t\"github.com/greenguavalabs/jujube/internal/cli\"\n",
        "\tcli.Run()\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestMainGoCustomSlug(SetupMainGo):
    @pytest.fixture
    def project_slug(self):
        return "jujube-new"

    def test_has_main_go(self, template, project_slug):
        assert template.project_path.joinpath(
            "cmd", project_slug, "main.go").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0


class TestMainGoNoCli(SetupMainGo):
    @pytest.fixture
    def context(self):
        return {"cli": "no"}

    def test_has_main_go(self, template):
        assert template.project_path.joinpath(
            "cmd", "jujube", "main.go").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "\t\"fmt\"\n",
        "\tfmt.Println(\"Hello World!\")\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
