# -*- coding: utf-8 -*-

import pytest


class SetupGoMod:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath("go.mod").open() as f:
            return f.readlines()


class TestGoModDefault(SetupGoMod):
    def test_has_makefile(self, template):
        assert template.project_path.joinpath("go.mod").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "module github.com/greenguavalabs/jujube\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestGoModProjectSlugCustom(SetupGoMod):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-new"}

    lines = [
        "module github.com/greenguavalabs/jujube-new\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
