# -*- coding: utf-8 -*-

import pytest


class SetupDevcontainerJson:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath(".devcontainer.json").open() as f:
            return f.readlines()


class TestDevcontainerJson(SetupDevcontainerJson):
    def test_has_file(self, template):
        assert template.project_path.joinpath(".devcontainer.json").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        '    "name": "JUJUBE",\n',
        '    "service": "jujube_devbox",\n',
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestDevcontainerJsonProjectSlugHasDashes(SetupDevcontainerJson):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-new"}

    lines = [
        '    "name": "JUJUBE_NEW",\n',
        '    "service": "jujube_new_devbox",\n',
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
