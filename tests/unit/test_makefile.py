# -*- coding: utf-8 -*-

import pytest


class SetupMakefile:
    @pytest.fixture()
    def context(self):
        return None

    @pytest.fixture()
    def template(self, cookies, context):
        if context is None:
            return cookies.bake()
        else:
            return cookies.bake(extra_context=context)

    @pytest.fixture()
    def file(self, template):
        with template.project_path.joinpath("Makefile").open() as f:
            return f.readlines()


class TestMakefileDefault(SetupMakefile):
    def test_has_makefile(self, template):
        assert template.project_path.joinpath("Makefile").is_file()

    def test_not_empty(self, file):
        assert len(file) is not 0

    lines = [
        "# DO NOT modify this file.\n",
        "DEVBOX_CONTAINER=jujube_devbox\n",
        "-include build/rules.mk\n",
        "-include build/host_env.mk\n"
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file


class TestMakefileProjectSlugHasDashes(SetupMakefile):
    @pytest.fixture()
    def context(self):
        return {"project_slug": "jujube-new"}

    lines = [
        "DEVBOX_CONTAINER=jujube_new_devbox\n",
    ]

    @pytest.mark.parametrize("line", lines)
    def test_contains_line(self, file, line):
        assert line in file
