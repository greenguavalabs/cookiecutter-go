# -*- coding: utf-8 -*-

import docker
import subprocess

import pytest


class TestDevbox:
    @pytest.fixture()
    def container_name(self, project_slug):
        return project_slug + "_devbox"

    @pytest.fixture()
    def image_name(self, project_slug, container_name):
        return project_slug + "_" + container_name

    def test_lifecycle(
            self, docker_client, template,  cleaner, container_name,
            image_name):
        # create container & image
        res = subprocess.run(["make", "init"], cwd=template.project_path)
        assert 0 == res.returncode

        # verify running container
        container = docker_client.containers.get(container_name)
        assert container.name == container_name
        assert container.status == "running"

        # verify image created
        assert image_name + ":latest" in container.image.tags

        # remove container & image
        res = subprocess.run(["make", "clean"], cwd=template.project_path)
        assert 0 == res.returncode

        # verify artifacts are gone
        with pytest.raises(docker.errors.NotFound):
            docker_client.containers.get(container_name)
        with pytest.raises(docker.errors.ImageNotFound):
            docker_client.images.get(image_name)

    def test_double_init(
            self, docker_client, template, cleaner, container_name):
        # start container
        res = subprocess.run(["make", "init"], cwd=template.project_path)
        assert 0 == res.returncode

        # verify container is running
        container = docker_client.containers.get(container_name)
        assert container.status == "running"

        # start container again
        res = subprocess.run(["make", "init"], cwd=template.project_path)
        assert 0 == res.returncode

        # verify container is running
        container = docker_client.containers.get(container_name)
        assert container.status == "running"

    def test_clean_when_container_does_not_exist(
            self, docker_client, template, container_name):
        # verify container does not exist
        with pytest.raises(docker.errors.NotFound):
            docker_client.containers.get(container_name)

        res = subprocess.run(["make", "clean"], cwd=template.project_path)
        assert 0 == res.returncode

    def test_no_target(self, docker_client, template, cleaner, container_name):
        res = subprocess.run(["make"], cwd=template.project_path)
        assert 0 == res.returncode

        # verify container is running
        container = docker_client.containers.get(container_name)
        assert container.status == "running"
