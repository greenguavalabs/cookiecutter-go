# -*- coding: utf-8 -*-

import subprocess

import pytest


class TestBuildTargets:
    targets = [
        # main dev targets
        (None, 0),
        ("all", 0),
        ("bins", 0),
        ("lint", 0),
        ("test", 0),

        # tools related targets
        ("tools", 0),
        ("target_does_not_exist", 2),
    ]

    @pytest.mark.parametrize("target, expRetCode", targets)
    def test_target(self, template, devbox, target, expRetCode):
        cmd = ["make"]
        if target is not None:
            cmd.append(target)

        res = subprocess.run(cmd, cwd=template.project_path)
        assert expRetCode == res.returncode

    def test_bins_target(self, template, devbox, project_slug):
        res = subprocess.run(["make", "bins"], cwd=template.project_path)
        assert 0 == res.returncode

        assert template.project_path.joinpath(
            "cmd", project_slug, project_slug).is_file()


class TestTestTarget:
    @pytest.fixture()
    def failing_covrc(self, template):
        subprocess.run(
            ["mv", "-f", ".covrc", ".covrc.orig"], cwd=template.project_path)
        p = template.project_path.joinpath(".covrc")
        with open(p, 'w') as covrc_file:
            covrc_file.write('fail_under 100\n')

        yield
        subprocess.run(
            ["mv", "-f", ".covrc.orig", ".covrc"], cwd=template.project_path)

    def test_coverage_below_threshold(self, template, devbox, failing_covrc):
        res = subprocess.run(["make", "test"], cwd=template.project_path)
        assert 2 == res.returncode
