# -*- coding: utf-8 -*-

import docker
import random
import string
import subprocess

import pytest
from pytest_cookies.plugin import Cookies


@pytest.fixture(scope="session")
def docker_client():
    return docker.from_env()


@pytest.fixture(scope="session")
def cookies(request, tmpdir_factory, _cookiecutter_config_file):
    """Yield an instance of the Cookies helper class that can be used to
    generate a project from a template.
    Run cookiecutter:
        result = cookies.bake(extra_context={
            'variable1': 'value1',
            'variable2': 'value2',
        })

    COPIED from pytest_cookies and redefined for session scope
    """
    template_dir = request.config.option.template

    output_dir = tmpdir_factory.mktemp('cookies')
    output_factory = output_dir.mkdir

    yield Cookies(
        template_dir, output_factory, _cookiecutter_config_file)

    output_dir.remove()


@pytest.fixture(scope="module")
def project_slug():
    slug = [random.choice(string.ascii_letters) for n in range(10)]
    return "".join(slug).lower()


@pytest.fixture(scope="module")
def context(project_slug):
    return {
        "project_slug": project_slug,
        "_test_hooks": "yes",
    }


@pytest.fixture(scope="module")
def template(cookies, context):
    return cookies.bake(extra_context=context)


@pytest.fixture(scope="module")
def devbox(template):
    subprocess.run(["make", "init"], cwd=template.project_path)
    yield
    subprocess.run(["make", "clean"], cwd=template.project_path)


@pytest.fixture()
def cleaner(template):
    yield
    subprocess.run(
        ["make", "clean"],
        cwd=template.project_path)
