package greet_test

import (
	"bytes"

	"github.com/spf13/cobra"

	. "{{cookiecutter.project_go_import_path}}/internal/cli/command/greet"
)

var _ = Describe("Greet", func() {
	var (
		out bytes.Buffer
		cmd *cobra.Command
	)

	BeforeEach(func() {
		out.Reset()

		cmd = &cobra.Command{}
		cmd.SetOutput(&out)
		cmd.Flags().StringP(NameFlag, NameFlagShorthand, "", "")
		err := cmd.ParseFlags(WithNameFlag("test")([]string{}))
		Expect(err).ToNot(HaveOccurred())
	})

	It("returns result with greeting", func() {
		res, err := Greet(cmd, []string{})
		Expect(err).ToNot(HaveOccurred())

		Expect(res).To(Equal(&Output{
			Message: "hello test",
		}))
	})

	It("prints greeting", func() {
		_, err := Greet(cmd, []string{})
		Expect(err).ToNot(HaveOccurred())

		Expect(out.String()).To(Equal("hello test\n"))
	})

	Context("when 'name' not defined on command", func() {
		BeforeEach(func() {
			cmd = &cobra.Command{}
		})

		It("fails with an error", func() {
			res, err := Greet(cmd, []string{})
			Expect(res).To(BeNil())
			Expect(err).To(MatchError(ContainSubstring("failed to retrieve 'name' flag")))
		})
	})

	Context("when 'name' value empty string", func() {
		BeforeEach(func() {
			err := cmd.ParseFlags(WithNameFlag("")(nil))
			Expect(err).ToNot(HaveOccurred())
		})

		It("fails with an error", func() {
			res, err := Greet(cmd, []string{})
			Expect(res).To(BeNil())
			Expect(err).To(MatchError(ContainSubstring("'name' is required")))
		})
	})
})
