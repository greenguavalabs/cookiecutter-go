package greet

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

type (
	// Output is the result produced by the 'greet' command.
	Output struct {
		Message string `json:"msg"`
	}
)

// Greet implements the action to be performed for a 'greet' command.
func Greet(cmd *cobra.Command, args []string) (interface{}, error) {
	result := &Output{}

	name, err := cmd.Flags().GetString(NameFlag)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to retrieve '%s' flag", NameFlag)
	}
	if len(name) == 0 {
		return nil, errors.Errorf("'%s' is required", NameFlag)
	}

	result.Message = fmt.Sprintf("hello %s", name)
	fmt.Fprintf(cmd.OutOrStdout(), "%s\n", result.Message)

	return result, nil
}
