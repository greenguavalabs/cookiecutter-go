package greet_test

import (
	"fmt"

	"github.com/onsi/ginkgo/extensions/table"
	"github.com/spf13/cobra"

	. "{{cookiecutter.project_go_import_path}}/internal/cli/command/greet"
)

var _ = Describe("Register", func() {
	var (
		cmd *cobra.Command
	)

	BeforeEach(func() {
		cmd = &cobra.Command{}
	})

	It("adds greet command", func() {
		err := Register(cmd)
		Expect(err).ToNot(HaveOccurred())

		Expect(cmd.Commands()).To(HaveLen(1))
		Expect(cmd.Commands()[0].Use).To(Equal(CmdName))
	})

	table.DescribeTable("adds greet command flags",
		func(name, short string) {
			err := Register(cmd)
			Expect(err).ToNot(HaveOccurred())
			Expect(cmd.Commands()).To(HaveLen(1))

			greetCmd := cmd.Commands()[0]
			Expect(greetCmd.Flag(name)).ToNot(BeNil())
			if len(short) != 0 {
				Expect(greetCmd.LocalFlags().ShorthandLookup(NameFlagShorthand)).ToNot(BeNil())
			}
		},
		table.Entry(fmt.Sprintf("'%s' flag", NameFlag), NameFlag, NameFlagShorthand),
	)

	Context("when flag name is already used on parent command", func() {
		table.DescribeTable("fails with an error",
			func(name string) {
				// create conflict by adding flag to parent
				cmd.PersistentFlags().String(name, "", "")

				err := Register(cmd)
				Expect(err).To(MatchError(fmt.Sprintf("flag name '%s' or shorthand 'n' already in use", name)))
			},
			table.Entry(fmt.Sprintf("'%s' flag", NameFlag), NameFlag),
		)
	})

	Context("when flag shorthand is already used on parent command", func() {
		table.DescribeTable("fails with error",
			func(short string) {
				// create conflict by adding flag to parent
				cmd.PersistentFlags().StringP("foo", short, "", "")

				err := Register(cmd)
				Expect(err).To(MatchError(fmt.Sprintf("flag name 'name' or shorthand '%s' already in use", short)))
			},
			table.Entry(fmt.Sprintf("'%s' shorthand", NameFlagShorthand), NameFlagShorthand),
		)
	})
})
