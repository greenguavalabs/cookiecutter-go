package greet

import(
	"fmt"

	"github.com/spf13/cobra"

	"{{cookiecutter.project_go_import_path}}/internal/cli/action"
)

const (
	// CmdName is the name of the command.
	CmdName = "greet"

	// NameFlag is the name of the flag used to pass the message.
	NameFlag = "name"

	// NameFlagShorthand is the shorthand value for NameFlag.
	NameFlagShorthand = "n"
)

// WithNameFlag returns a flag value setter for NameFlag.
func WithNameFlag(value string) func([]string) []string {
	return func(args []string) []string {
		return append(args, fmt.Sprintf("--%s", NameFlag), value)
	}
}

// Register configures a 'greet' command as a child command.
func Register(cmd *cobra.Command) error {
	greet := newCommand()
	cmd.AddCommand(greet)

	err := addFlags(greet)
	if err != nil {
		return err
	}

	return nil
}

func newCommand() *cobra.Command {
	return &cobra.Command{
		Use: CmdName,
		RunE: action.RunE(Greet),
	}
}

func addFlags(cmd *cobra.Command) error {
	err := action.ValidateCmdFlag(cmd, NameFlag, NameFlagShorthand)
	if err != nil {
		return err
	}
	cmd.Flags().StringP(
		NameFlag,
		NameFlagShorthand,
		"",
		"name of person to greet",
	)

	return nil
}
