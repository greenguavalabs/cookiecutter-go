package command_test

import (
	"github.com/spf13/cobra"

	"{{cookiecutter.project_go_import_path}}/internal/cli/action"
	. "{{cookiecutter.project_go_import_path}}/internal/cli/command"
	"{{cookiecutter.project_go_import_path}}/internal/cli/command/greet"
)

var _ = Describe("NewCommand", func() {
	It("returns initialized instance", func() {
		cmd := NewCommand()
		Expect(cmd).ToNot(BeNil())
		Expect(cmd.Use).To(Equal("{{cookiecutter.project_slug}}"))
		Expect(cmd.Short).To(Equal("{{cookiecutter.project_slug}} app"))
		Expect(cmd.Long).To(Equal("{{cookiecutter.project_slug}} CLI application"))
	})
})

var _ = Describe("Initialize", func() {
	var (
		cmd *cobra.Command
	)

	BeforeEach(func() {
		cmd = &cobra.Command{}
	})

	It("configures action flags", func() {
		res, err := Initialize(cmd)
		Expect(err).ToNot(HaveOccurred())
		Expect(res.Flag(action.OutputModeFlag)).ToNot(BeNil())
	})

	It("adds 'greet' sub command", func() {
		res, err := Initialize(cmd)
		Expect(err).ToNot(HaveOccurred())
		Expect(res.Commands()).To(HaveLen(1))

		greetCmd, _, err := res.Find([]string{"greet"})
		Expect(err).ToNot(HaveOccurred())
		Expect(greetCmd.Name()).To(Equal("greet"))
	})

	Context("when adding 'action' flags fails", func() {
		BeforeEach(func() {
			cmd.Flags().StringP(action.OutputModeFlag, "", "", "")
		})

		It("propagates the error", func() {
			res, err := Initialize(cmd)
			Expect(res).To(BeNil())
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(HavePrefix("failed to configure action flags"))
		})
	})

	Context("when registering 'greet' subcommand fails", func() {
		BeforeEach(func() {
			// create a flag that will conflict withe the flag added by the
			// 'greet' command
			cmd.PersistentFlags().StringP(greet.NameFlag, "", "", "")
		})

		It("propagates the error", func() {
			res, err := Initialize(cmd)
			Expect(res).To(BeNil())
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(HavePrefix("failed to register 'greet' command"))
		})
	})
})
