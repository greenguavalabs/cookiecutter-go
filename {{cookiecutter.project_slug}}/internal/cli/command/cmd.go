package command

import (
	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"{{cookiecutter.project_go_import_path}}/internal/cli/action"
	"{{cookiecutter.project_go_import_path}}/internal/cli/command/greet"
)

// NewCommand initializes the root command.
func NewCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "{{cookiecutter.project_slug}}",
		Short: "{{cookiecutter.project_slug}} app",
		Long:  "{{cookiecutter.project_slug}} CLI application",
	}
}

// Initialize configures cmd as the root command for the {{cookiecutter.project_slug}}
// application.
func Initialize(cmd *cobra.Command) (*cobra.Command, error) {
	err := action.AddFlags(cmd)
	if err != nil {
		return nil, errors.Wrap(err, "failed to configure action flags")
	}

	err = greet.Register(cmd)
	if err != nil {
		return nil, errors.Wrap(err, "failed to register 'greet' command")
	}

	return cmd, nil
}
