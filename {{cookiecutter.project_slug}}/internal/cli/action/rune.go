package action

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

type (
	// RunEAction defines the function signature for the action to be wrapped by
	// RunE.
	RunEAction func(cmd *cobra.Command, args []string) (interface{}, error)
)

// RunE implements a wrapper for an action function that returns an error.
func RunE(
	action RunEAction,
) (func(cmd *cobra.Command, args []string) error) {
	return func(cmd *cobra.Command, args []string) error {
		return runE(action, cmd, args)
	}
}

func runE(action RunEAction, cmd *cobra.Command, args []string) error {
	mode, err := cmd.Flags().GetString(OutputModeFlag)
	if err != nil {
		return errors.Wrap(err, "failed to determine output mode")
	}

	switch mode {
	case OutputModeStandard:
		err = runEStandard(action, cmd, args)
	case OutputModeJSON:
		err = runEJSON(action, cmd, args)
	default:
		err = errors.Errorf("unsupported output mode: '%s'", mode)
	}
	return err
}

func runEStandard(action RunEAction, cmd *cobra.Command, args []string) error {
	_, err := action(cmd, args)
	return err
}

func runEJSON(action RunEAction, cmd *cobra.Command, args []string) error {
	// attempt to silence cmd output
	sout, eout, reset := silenceCmdOutput(cmd)
	defer reset()

	res, err := action(cmd, args)
	if err != nil {
		err2 := printfJSONError(eout, err)
		cmd.SilenceErrors = (err2 == nil)
		cmd.SilenceUsage = (err2 == nil)
		return errors.Wrap(err, "failed to print action error as JSON")
	}

	err = printfJSON(sout, res)
	if err != nil {
		return errors.Wrap(err, "failed to print action result as JSON")
	}
	return nil
}

func silenceCmdOutput(cmd *cobra.Command) (io.Writer, io.Writer, func()) {
	sout := cmd.OutOrStdout()
	eout := cmd.OutOrStderr()
	cmd.SetOutput(ioutil.Discard)

	return sout, eout, func() {
		if sout == os.Stdout {
			cmd.SetOutput(nil)
			return
		}
		cmd.SetOutput(sout)
	}
}

func printfJSONError(writer io.Writer, err error) error {
	errJSON := struct {
		Error string `json:"error"`
	} {
		Error: err.Error(),
	}
	return printfJSON(writer, errJSON)
}

func printfJSON(writer io.Writer, data interface{}) error {
	rep, err := json.Marshal(data)
	if err != nil {
		return err
	}

	fmt.Fprintf(writer, string(rep))
	return nil
}
