package action_test

import (
	"github.com/spf13/cobra"

	. "{{cookiecutter.project_go_import_path}}/internal/cli/action"
)

var _ = Describe("WithOutputModeFlag", func() {
	It("returns an flag value setter", func() {
		var args []string
		args = WithOutputModeFlag("foo")(args)
		Expect(args).To(ConsistOf("--"+OutputModeFlag, "foo"))
	})
})

var _ = Describe("AddFlags", func() {
	var (
		cmd *cobra.Command
	)

	BeforeEach(func() {
		cmd = &cobra.Command{}
	})

	Describe("OutputModeFlag", func() {
		It("adds flag", func() {
			err := AddFlags(cmd)
			Expect(err).NotTo(HaveOccurred())

			flag := cmd.Flag(OutputModeFlag)
			Expect(flag).NotTo(BeNil())
			Expect(flag.Name).To(Equal(OutputModeFlag))
			Expect(flag.Shorthand).To(Equal(OutputModeFlagShorthand))
			Expect(flag.Value.Type()).To(Equal("string"))
			Expect(flag.Value.String()).To(Equal(OutputModeStandard))
		})

		Context("when flag name is in use by local flag", func() {
			BeforeEach(func() {
				cmd.Flags().String(OutputModeFlag, "", "")
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})

		Context("when flag name is in use by local persistent flag", func() {
			BeforeEach(func() {
				cmd.PersistentFlags().String(OutputModeFlag, "", "")
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})

		Context("when flag name is in use by persistent flag on parent", func() {
			BeforeEach(func() {
				pCmd := &cobra.Command{}
				pCmd.PersistentFlags().String(OutputModeFlag, "", "")
				pCmd.AddCommand(cmd)
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})

		Context("when flag shortname is already assigned to local flag", func() {
			BeforeEach(func() {
				cmd.Flags().StringP("foo", OutputModeFlagShorthand, "", "")
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})

		Context("when flag shortname is already assigned to local persistent flag", func() {
			BeforeEach(func() {
				cmd.PersistentFlags().StringP("foo", OutputModeFlagShorthand, "", "")
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})

		Context("when flag shortname is already assigned to flag on parent", func() {
			BeforeEach(func() {
				pCmd := &cobra.Command{}
				pCmd.PersistentFlags().StringP("foo", OutputModeFlagShorthand, "", "")
				pCmd.AddCommand(cmd)
			})

			It("returns an error", func() {
				err := AddFlags(cmd)
				Expect(err).To(MatchError("flag name 'output' or shorthand 'o' already in use"))
			})
		})
	})
})
