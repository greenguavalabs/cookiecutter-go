package action_test

import (
	"bytes"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/pkg/errors"

	. "{{cookiecutter.project_go_import_path}}/internal/cli/action"
)

var _ = Describe("RunE", func() {
	var (
		out bytes.Buffer
		cmd *cobra.Command

		action RunEAction
	)

	BeforeEach(func() {
		out.Reset()

		cmd = &cobra.Command{}
		cmd.SetOutput(&out)
		AddFlags(cmd)

		action = func(cmd *cobra.Command, args []string) (interface{}, error) {
			if len(args) == 0 {
				return nil, errors.New("action failed")
			}

			fmt.Fprintf(cmd.OutOrStdout(), "action says: %s\n", args[0])
			return &args[0], nil
		}
	})

	Context("when it cannot determine output mode", func() {
		It("returns an error", func() {
			err := RunE(action)(cmd, []string{"hello"})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(HavePrefix("failed to determine output mode"))
		})
	})

	Context("when output mode is unsupported", func() {
		BeforeEach(func() {
			cmd.ParseFlags(
				WithOutputModeFlag("unknown-mode")([]string{}))
		})

		It("returns an error", func() {
			err := RunE(action)(cmd, []string{"hello"})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal("unsupported output mode: 'unknown-mode'"))
		})
	})

	Context("when ouput mode is 'standard'", func() {
		BeforeEach(func() {
			cmd.ParseFlags(
				WithOutputModeFlag(OutputModeStandard)([]string{}))
		})

		It("executes the action", func() {
			err := RunE(action)(cmd, []string{"hello"})
			Expect(err).NotTo(HaveOccurred())
			Expect(out.String()).To(Equal("action says: hello\n"))
		})

		It("propagates the action's error", func() {
			err := RunE(action)(cmd, nil)
			Expect(err.Error()).To(Equal("action failed"))
		})
	})

	Context("when output mode is 'json'", func() {
		BeforeEach(func() {
			cmd.ParseFlags(
				WithOutputModeFlag(OutputModeJSON)([]string{}))
		})

		It("executes the action and surpresses action output", func() {
			err := RunE(action)(cmd, []string{"hello"})
			Expect(err).NotTo(HaveOccurred())
			Expect(out.String()).To(MatchJSON("\"hello\"\n"))
		})

		It("propagates the action's error", func() {
			err := RunE(action)(cmd, nil)
			Expect(err).To(HaveOccurred())
			Expect(out.String()).To(MatchJSON(`{"error":"action failed"}`))
		})

		It("sets cmd.SilenceErrors on action error", func() {
			err := RunE(action)(cmd, nil)
			Expect(err).To(HaveOccurred())
			Expect(cmd.SilenceErrors).To(BeTrue())
		})

		It("sets cmd.SilenceUsage on action error", func() {
			err := RunE(action)(cmd, nil)
			Expect(err).To(HaveOccurred())
			Expect(cmd.SilenceUsage).To(BeTrue())
		})

		It("resets the command output writer", func() {
			err := RunE(action)(cmd, []string{"hello"})
			Expect(err).NotTo(HaveOccurred())
			Expect(cmd.OutOrStdout()).To(BeIdenticalTo(&out))
		})

		Context("when action result is not serializable", func() {
			BeforeEach(func() {
				action = func(cmd *cobra.Command, args []string) (interface{}, error) {
					return make(chan int), nil
				}
			})

			It("returns the serialization error", func() {
				err := RunE(action)(cmd, nil)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(HavePrefix("failed to print action result as JSON"))
			})
		})
	})
})
