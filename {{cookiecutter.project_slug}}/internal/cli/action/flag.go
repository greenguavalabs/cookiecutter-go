package action

import (
	"fmt"

	"github.com/spf13/cobra"
)

const (
	// OutputModeFlag is the name for the flag that allows user to specify the
	// desired output mode.
	OutputModeFlag = "output"

	// OutputModeFlagShorthand is the shorthand value for the OutputModeFlag.
	OutputModeFlagShorthand = "o"
)

// Valid values for OutputModeFlag.
const (
	OutputModeStandard = "standard"
	OutputModeJSON = "json"
)

// WithOutputModeFlag returns a flag value setter for OutputModeFlag.
func WithOutputModeFlag(value string) func([]string) []string {
	return func(args []string) []string {
		return append(args, fmt.Sprintf("--%s", OutputModeFlag), value)
	}
}

// AddFlags adds action specific flags to the cmd parameter.
func AddFlags(cmd *cobra.Command) error {
	err := ValidateCmdFlag(cmd, OutputModeFlag, OutputModeFlagShorthand)
	if err != nil {
		return err
	}

	cmd.PersistentFlags().StringP(
		OutputModeFlag,
		OutputModeFlagShorthand,
		OutputModeStandard,
		fmt.Sprintf(
			"Command output mode one of [%s, %s]", OutputModeStandard, OutputModeJSON),
	)

	return nil
}

// ValidateCmdFlag checks if the name or shorthand is already assigned.
func ValidateCmdFlag(cmd *cobra.Command, name, shorthand string) error {
	if cmd.Flag(name) != nil ||
		cmd.LocalFlags().ShorthandLookup(shorthand) != nil ||
		cmd.InheritedFlags().ShorthandLookup(shorthand) != nil {
			return fmt.Errorf("flag name '%s' or shorthand '%s' already in use", name, shorthand)
	}

	return nil
}
