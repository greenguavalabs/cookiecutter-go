package cli

import (
	"fmt"
	"os"

	"{{cookiecutter.project_go_import_path}}/internal/cli/command"
)

// Run implements the execution entry point for the CLI app.
func Run() {
	cmd, err := command.Initialize(command.NewCommand())
	if err != nil {
		fmt.Printf("Error: failed to initilize: %v", err.Error())
		os.Exit(1)
	}
	if err := cmd.Execute(); err != nil {
	  os.Exit(1)
	}
}
