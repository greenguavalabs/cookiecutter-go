package main

import (
{% if cookiecutter.cli == 'yes' %}
	"{{ cookiecutter.project_go_import_path }}/internal/cli"
{% else %}
	"fmt"
{% endif %}
)

func main() {
{% if cookiecutter.cli == 'yes' %}
	cli.Run()
{% else %}
	fmt.Println("Hello World!")
{% endif %}
}
