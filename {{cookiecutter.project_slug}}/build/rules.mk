# make targets to be executed inside the devbox container.
#
# These are primarily build targets.
#

.PHONY: all
all: bins lint test

PROGS = cmd/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }}

.PHONY: bins
bins: $(PROGS)

$(PROGS): go_mod generate
	$(GO) build -o $@ $(shell cd $(dir $@) && go list -e)

.PHONY: go_mod
go_mod:
	$(GO) mod tidy

.PHONY: generate
generate: tools
	$(GO) generate ./...

.PHONY: lint
lint: tools go_mod generate
	$(GOLINT) -set_exit_status $(shell go list ./... | grep -v /vendor/)
	$(GO) vet ./...
	$(GO) fmt ./...

.PHONY: test
test: go_mod generate coverage
	$(GO) test -coverprofile="$(COVERAGE_FILE)" -race -v ./...
	$(GO) tool cover -func="$(COVERAGE_FILE)"
	$(GO) run build/tools/covcheck.go -coverprofile "$(COVERAGE_FILE)" -threshold $(COV_THRESHOLD)

# Rules to install tools needed for build.
-include build/tools.mk
