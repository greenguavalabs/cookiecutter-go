# make targets to be executed from outside the devbox container.
#
# Add any additional targets that you want to be runable from the host into this
# file.
#

DOCKER_CMD = docker exec -t $(DEVBOX_CONTAINER) bash -c "cd /src && $(1)"

.DEFAULT:
	$(MAKE) init
	$(call DOCKER_CMD,make $@)
{% if cookiecutter._test_hooks == 'yes' %}
	$(call DOCKER_CMD,ls -a $(shell pwd) && cp -r -u . $(shell pwd) && ls -a && ls -a $(shell pwd))
{% endif %}

.PHONY: all
all: init
	$(call DOCKER_CMD,make)
{% if cookiecutter._test_hooks == 'yes' %}
	$(call DOCKER_CMD,ls -a $(shell pwd) && cp -r -u . $(shell pwd) && ls -a && ls -a $(shell pwd))
{% endif %}

# Rules to setup and tear down the development environment.
DOCKER_COMPOSE_CMD = docker-compose -f deployments/docker/docker-compose.yml -p {{ cookiecutter.project_slug.replace('-', '_') }}

.PHONY: init
init: .tmp/init
{% if cookiecutter._test_hooks == 'yes' %}
	$(call DOCKER_CMD,ls -a $(shell pwd) && cp -r $(shell pwd)/. . && ls -a)
{% endif %}

.tmp/init:
	$(DOCKER_COMPOSE_CMD) up -d
	mkdir -p ./.tmp && touch ./.tmp/init

.PHONY: clean
clean:
	$(DOCKER_COMPOSE_CMD) kill
	$(DOCKER_COMPOSE_CMD) down --rmi local
	rm -f ./.tmp/init

{% if cookiecutter._test_hooks == 'yes' %}
.PHONY: docker_cmd
docker_cmd:
	@echo $(DOCKER_CMD)
{% endif %}
