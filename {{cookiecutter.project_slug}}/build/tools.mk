# make targets to wrap common dep commands and tools

GO=GO111MODULE=on go
GO_OFF=GO111MODULE=off go
GOLINT=$(GO)lint

.PHONY: tools
tools: \
		$(GOPATH)/bin/golint \
		$(GOPATH)/bin/mockgen

$(GOPATH)/bin/golint:
	cd $(GOPATH) && $(GO) install golang.org/x/lint/golint@latest

$(GOPATH)/bin/mockgen:
	cd $(GOPATH) && $(GO) install github.com/golang/mock/mockgen@latest

# coverage checker
COVERAGE_DIR=.cover
COVERAGE_FILE=$(COVERAGE_DIR)/cover.out
COV_THRESHOLD=$(shell cat .covrc | grep "fail_under" | cut -d ' ' -f2)

$(COVERAGE_DIR):
	mkdir -p $(COVERAGE_DIR)

coverage: $(COVERAGE_DIR)
