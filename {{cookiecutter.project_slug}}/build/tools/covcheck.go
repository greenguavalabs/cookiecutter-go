package main

import (
	"flag"
	"fmt"
	"os"

	"golang.org/x/tools/cover"
)

var (
	coverprofile = flag.String("coverprofile", "", "Coverage profile file generate by 'go test'")
	threshold = flag.Float64("threshold", 0.0, "Minimum coverage threshold")
)

func main() {
	flag.Parse()
	if *coverprofile == "" {
		fmt.Fprintln(os.Stderr, "Coverage profile file was not specified. Specify it using the '-coverprofile' flag")
		os.Exit(2)
	}

	coverage, err := calcCoverage(*coverprofile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(2)
	}

	fmt.Printf("Calculated coverage %.2f\n", coverage)
	if coverage+.001 < *threshold {
		fmt.Printf("Code coverage %.2f less than required %.2f\n", coverage, *threshold)
		os.Exit(1)
	}
}

func calcCoverage(coverprofile string) (float64, error) {
	profiles, err := cover.ParseProfiles(coverprofile)
	if err != nil {
		return 0.0, fmt.Errorf("cannot parse coverage profile file %s", coverprofile)
	}

	total := 0
	covered := 0
	for _, profile := range profiles {
		for _, block := range profile.Blocks {
			total += block.NumStmt
			if block.Count > 0 {
				covered += block.NumStmt
			}
		}
	}

	if total == 0 {
		return 0.0, nil
	}
	return float64(covered) / float64(total) * 100, nil
}
