# cookiecutter-go

A template to generate a new golang project

## Usage

### Using locally installed python

Switch to a project folder and generate the `cookiecutter` template:

```bash
pip install cookiecutter && cookiecutter bb:greenguavalabs/cookiecutter-go.git
```
### Using python docker container

Switch to a project folder and generate the `cookiecutter` template:

```bash
docker run -v `pwd`:/src -it python bash -c "cd /src && pip install cookiecutter && cookiecutter bb:greenguavalabs/cookiecutter-go.git"
```

## Features

### Development environment

The `cookiecutter-go` template generates a Docker based development environment for `golang` projects. The following `make` tasks are available to operate the dev env.

Manage the development environment:

* `make init` - Bring up the docker container
* `make clean` - Stop and remove docker container

Verify and test project code:

* `make bins` - Build all project binaries
* `make lint` - Run lint tools on all project files
* `make test` - Run all project test

### CLI

The `cookiecutter-go` template provides you the option to generate `golang` packages for building a command line app using the [github.com/spf13/cobra](http://github.com/spf13/cobra) framework.

To generate the **cli** packages chose **1** at the prompt below:

```bash
Select cli:
1 - yes
2 - no
Choose from 1, 2 [1]:
```

## License

This project is licensed under the terms of the [MIT License](/LICENSE)
