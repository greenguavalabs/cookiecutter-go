# -*- coding: utf-8 -*-

"""Implements post-gen hooks for the cookiecutter-go template."""

from collections import OrderedDict
import hashlib
import json
import logging
import shutil
import os


def ensure_cli_package():
    """Ensure cli package presence according to user selection."""
    enbled = "{{ cookiecutter.cli }}"

    if enbled != "yes":
        shutil.rmtree("internal/cli")


def gen_config_file():
    """Generate config file with the captured input values."""
    context = {{cookiecutter}}

    with open("cookiecutter.json", "w") as outfile:
        json.dump(context, outfile)


def gen_file_manifest():
    """Generate a manifest files storing CRC for all generated files."""
    project_dir = "."
    manifest_file = ".cookiecutter.crc"

    with open(manifest_file, "w") as manifest:
        for root, dirs, files in os.walk(project_dir):
            for file in files:
                if file == manifest_file:
                    # skip over the manifest file
                    continue

                file_path = os.path.join(root, file)
                file_crc = hashlib.sha256(
                    open(file_path, 'rb').read()).hexdigest()
                manifest.write("{}|{}\n".format(file_crc, file_path))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    LOG = logging.getLogger('post_gen_project')

    ensure_cli_package()
    gen_config_file()
    gen_file_manifest()
